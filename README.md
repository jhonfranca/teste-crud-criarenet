# teste-crud-criarenet

Projeto de teste CRUD para empresa Criarenet

## Início

Para utilizar a aplicação voce deve seguir os seguintes passo a passo:

## Passo a Passo

1 - Faça um clone da branch "MASTER"

2 - Instale [json-server](https://github.com/typicode/json-server)

3 - Abra o terminal

4 - Use o comando 'npm install -g json-server' para instalar

5 - Vá até a pasta do projeto (teste-crud-criarenet/api)

6 - Dentro dessa pasta, rode a aplicação com o comando 'json-server --watch db.json'

7 - Acesse a pasta 'ui' dentro da pasta do projeto

8 - Abra index.html

9 - Se o banco de dados não carregar, acesso ui/variables.js e altere o valor da variável de acordo com o endereço onde o json-server está rodando.
