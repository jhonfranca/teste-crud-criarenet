const users={template: `
<div>

<button type="button" class="btn btn-primary m-2 fload-end"
data-bs-toggle="modal"
data-bs-target="#genericModal"
@click="addClick()">
Cadastrar Usuário
</button>

<table class="table table-stripped">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Departamento
            </th>
            <th>
            Cpf
            </th>
            <th>
                Nome
            </th>
            <th>
                Email
            </th>
            <th>
                Telefone
            </th>
            <th>
                Opções
            </th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="user in users">
            <td>{{user.id}}</td>
            <td>{{user.userdepartment}}</td>
            <td>{{user.cpf}}</td>
            <td>{{user.name}}</td>
            <td>{{user.email}}</td>
            <td>{{user.phone}}</td>
            <td>
                <button type="button"
                    class="btn btn-light mr-1"
                    data-bs-toggle="modal"
                    data-bs-target="#genericModal"
                    @click="editClick(user)">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                </button>
                <button type="button" @click="deleteClick(user.id)"
                    class="btn btn-light mr-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                    </svg>
                </button>
            </td>
        </tr>
    </tbody>
</table>

<div class="modal fade" id="genericModal" tabindex="-1" aria-labelledby="genericModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="genericModalLabel">{{modalTitle}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
            <div class="d-flex flex-row bd-highlight mb-3">
                <div class="p-2 w-50 bd-highlight">
                    <div class="input-group mb-3">
                        <span class="input-group-text">Departamento</span>
                        <select class="form-select" v-model="userdepartment">
                            <option v-for="dep in department">
                                {{dep.DepartmentName}}
                            </option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Cpf</span>
                        <input type="text" class="form-control" v-model="cpf"></input>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Nome</span>
                        <input type="text" class="form-control" v-model="UserName"></input>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Email</span>
                        <input type="text" class="form-control" v-model="email"></input>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Telefone</span>
                        <input type="text" class="form-control" v-model="phone"></input>
                    </div>
                </div>
            </div>
                <button type="button" @click="createClick()" v-if="UserId==0" class="btn btn-primary">
                    Cadastrar Usuário
                </button>
                <button type="button" @click="updateClick()" v-if="UserId!=0" class="btn btn-primary">
                    Editar Usuário
                </button>
            </div>
        </div>
    </div>

</div>
 

</div>
`,

data (){
    return{
        users:[],
        modalTitle:"",
        userdepartment:"",
        department:"",
        UserName:"",
        UserId:0,
        cpf:0,
        email:"",
        phone:0

    }
},
methods: {
    refreshData(){
        axios.get(variables.API_URL+"users")
        .then((response)=>{
            this.users=response.data;
        });
        axios.get(variables.API_URL+"department")
        .then((response)=>{
            this.department=response.data;
        });
    },
    addClick(){
        this.modalTitle="Cadastrar Usuário";
        this.UserId=0;
        this.cpf=0;
        this.UserName="";
        this.email="";
        this.phone=0;
        this.userdepartment="";
    },
    editClick(user){
        this.modalTitle="Editar Usuário";
        this.UserId=user.id;
        this.userdepartment=user.userdepartment;
        this.cpf=user.cpf;
        this.UserName=user.name;
        this.email=user.email;
        this.phone=user.phone;
    },
    createClick(){
        axios.post(variables.API_URL+"users",{
            userdepartment:this.userdepartment,
            cpf:this.cpf,
            name:this.UserName,
            email:this.email,
            phone:this.phone

        })
        .then((response)=>{
            this.refreshData();
            alert("CRIADO COM SUCESSO!");
        });
    },
    updateClick(){
        axios.put(variables.API_URL+"users/"+this.UserId,{
            UserId:this.UserId,
            userdepartment:this.userdepartment,
            cpf:this.cpf,
            name:this.UserName,
            email:this.email,
            phone:this.phone
        })
        .then((response)=>{
            this.refreshData();
            alert("EDITADO COM SUCESSO");
        });
    },
    deleteClick(id){
        if(!confirm("QUER MESMO DELETAR?")){
            return;
        }
        axios.delete(variables.API_URL+"users/"+id)
        .then((response)=>{
            this.users=response.data;
            this.refreshData();
            alert("DELETADO COM SUCESSO");
        });

    }
},
mounted:function(){
    this.refreshData();
}

}