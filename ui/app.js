const routes=[
    {path: '/',component:home},
    {path: '/department',component:department},
    {path: '/users',component:users}
]

const router=new VueRouter({
    routes
})

const app = new Vue({
    router
}).$mount('#app')
