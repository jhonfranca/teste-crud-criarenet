const department={template: `
<div>

<button type="button" class="btn btn-primary m-2 fload-end"
data-bs-toggle="modal"
data-bs-target="#genericModal"
@click="addClick()">
Cadastrar Departamento
</button>

<table id="idtabela" class="table table-stripped">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Nome
            </th>
            <th>
                Opções
            </th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="dep in departments">
            <td>{{dep.id}}</td>
            <td>{{dep.DepartmentName}}</td>
            <td>
                <button type="button"
                    class="btn btn-light mr-1"
                    data-bs-toggle="modal"
                    data-bs-target="#genericModal"
                    @click="editClick(dep)">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                </button>
                <button type="button" @click="deleteClick(dep.id)"
                    class="btn btn-light mr-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                    </svg>
                </button>
            </td>
        </tr>
    </tbody>
    </table>
    <div>
    <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul>
    </nav>
    </div>


<div class="modal fade" id="genericModal" tabindex="-1" aria-labelledby="genericModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="genericModalLabel">{{modalTitle}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="input-group mb-3">
                    <span class="input-group-text">Nome do Departamento</span>
                    <input type="text" class="form-control" v-model="DepartmentName"></input>
                </div>

                <button type="button" @click="createClick()" v-if="DepartmentId==0" class="btn btn-primary">
                    Cadastrar Departamento
                </button>
                <button type="button" @click="updateClick()" v-if="DepartmentId!=0" class="btn btn-primary">
                    Editar Departamento
                </button>
            </div>
        </div>
    </div>

</div>
 

</div>
`,

data (){
    return{
        departments:[],
        modalTitle:"",
        DepartmentName:"",
        DepartmentId:0

    }
},
methods: {
    refreshData(){
        axios.get(variables.API_URL+"department")
        .then((response)=>{
            this.departments=response.data;
        });
    },
    addClick(){
        this.modalTitle="Cadastrar Departamento";
        this.DepartmentId=0;
        this.DepartmentName="";
    },
    editClick(dep){
        this.modalTitle="Editar Departamento";
        this.DepartmentId=dep.id;
        this.DepartmentName=dep.DepartmentName;
        console.log(this.DepartmentName, this.DepartmentId)
    },
    createClick(){
        axios.post(variables.API_URL+"department",{
            DepartmentName:this.DepartmentName
        })
        .then((response)=>{
            this.refreshData();
            alert("CRIADO COM SUCESSO!");
        });
    },
    updateClick(){
        axios.put(variables.API_URL+"department/"+this.DepartmentId,{
            DepartmentName:this.DepartmentName,
            DepartmentId:this.id
        })
        .then((response)=>{
            this.refreshData();
            alert("EDITADO COM SUCESSO");
        });
    },
    deleteClick(id){
        if(!confirm("QUER MESMO DELETAR?")){
            return;
        }
        axios.delete(variables.API_URL+"department/"+id)
        .then((response)=>{
            this.departments=response.data;
            this.refreshData();
            alert("DELETADO COM SUCESSO");
        });

    }
},
mounted:function(){
    this.refreshData();
}

}